/* YARB - Yet another RPG board
 *
 * Copyright (C) 2016 Michal Grochmal
 *
 * This page is free software, and is distributed under the terms of the GNU
 * General Public License version 3 or (at your option) any later version.
 */

/* All javascript here is encapsulated into micro-libraries,
 * the global namespace names used are:
 *   `window.FiFi`       - global object for linkage between libs
 *   `window.KeepState`  - wrapper over `localStorage` and utilities
 *   `window.DragScroll` - scroll by dragging (plus mobile workarounds)
 *   `window.EnumFields` - generate IDs for all fields
 *   `window.YarbField`  - flip the fields on the board
 *   `window.YarbFigure` - drag, drop and selection of figures
 *   `window.MoveBoard`  - moves the board out of the 16 fields limit
 *   `window.Dice`       - generate results for rolling dice
 *   `window.PageLinks`  - interface between the above and page anchors
 *   `window.Test`       - simple unit tests for the most crucial functions
 *
 * The naming convention used in the micro-libraries follow:
 *   `board` - The main pane containing `fields`.
 *   `field` - A single field with an ID, it may `face` `on` or `off` and it
 *             may contain a `figure`.
 *   `face` - The face of a `field`, fields containing `figures` are always
 *             `on`.
 *   `flip` - Act of changing the `face` of a `field`.
 *   `garrison` - The left pane containing several `figures`.
 *   `figure` - The image of a character or monster, figures in the `garrison`
 *              are copied when placed on the `board` but figures dragged on
 *              the `board` itself are `moved` between `fields`.
 *   `move` - The act of dragging or `selecting` and placing a `figure` on the
 *            `board`, moving from the `garrison` copies the `figure`.
 *   `remove` - Act of removing a `figure` from the board.
 *   `show` - The act of displaying a dialog.
 *   `hide` - The act of hiding a currently displayed dialog.
 *   `combat` - Typical use of YARB in tabletop RPG games, although other
 *              `scenes` may benefit from the use of YARB.
 *   `scene` - The current state of YARB, representing a piece of `combat`
 *             or other tabletop RPG even that benefits from spatial
 *             representation.
 */

// let's hope this does not break yet another shitty mobile browser
'use strict';

/* `FiFi` (fields and figures) is a global object that holds the correct way of
 * modifying the display.  At the beginning `FiFi` contains default values for
 * the HTML properties and `null` placeholders for the objects that modify the
 * HTML.  The `null` placeholders are later filled by objects that perform the
 * work on the internal state of YARB (`KeepState`) and on the HTML display.
 * These objects often reference the default HTML properties in `FiFi`,
 * therefore changing these default properties will have immediate impact.
 *
 * An elegant way of using `FiFi` is to use it as a placeholder in all places
 * where display and state modification is needed, and only fill in the
 * modifying objects when all state is ready for user interaction; this is a
 * form of late binding and allows objects to use all functionality before it
 * becomes available.  Of course, it is important to delay any work on the
 * display to after all modifying objects are bound.
 */
var FiFi = {};
FiFi.EVDEBUGON = false;  // debug for event listeners
FiFi.EVDEBUG   = function() {
  if (FiFi.EVDEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['FiFi.EVENT:'].concat(args));
  }
};
FiFi.fieldIdPrefix      = 'field';
FiFi.figureClass        = 'unit-image';
FiFi.figureUnknown      = 'misc-unknown-unit'
FiFi.fieldOnClass       = 'colordiv';
FiFi.fieldOffClass      = 'nocolordiv';
FiFi.figSelectedClass   = 'unit-image selected-unit';
FiFi.figDeselectedClass = 'unit-image';
FiFi.buttonOn           = 'message-button';
FiFi.buttonOff          = 'message-button-off';
FiFi.f = null;  // f.flipField(field, [forceOff]) : flip field
FiFi.m = null;  // m.mvFig(figure, field)         : move figure between fields
FiFi.r = null;  // r.rmFig(figure, field)         : remove figure from field
FiFi.i = null;  // i.isSelected([figure])         : is figure selected?
FiFi.s = null;  // s.select(figure)               : select the figure
FiFi.d = null;  // d.deselect([figure])           : deselect figure
FiFi.e = null;  // e.rndMsg(x, y, ...)            : displays an error message

/* `KeepState` is a wrapper over `localStorage`, its functions shall be called
 * by all other micro-libraries when updating any state that needs to be saved.
 * This also means that no other library shall call any `localStorage`
 * routines, which, in the long term, may allow for the use of different kinds
 * of storage.
 *
 * `KeepState` also contains functions to clear and draw the entire board,
 * these are needed if the entire state of the board was updated.
 *
 * The storage uses IDs of the form `field:row:column` where row and column
 * counts start at zero (e.g. field:2:6 for 3rd row and 7th column).  Passing
 * fields without an ID component to `KeepState` is likely to break it.
 */
var KeepState = {};
KeepState.DEBUGON = false;
KeepState.DEBUG   = function() {
  if (KeepState.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['KeepState:'].concat(args));
  }
};
KeepState.isField = function(id) {
  var re = new RegExp('^' + FiFi.fieldIdPrefix + ':\\d+:\\d+$');
  return !!re.exec(id);
};
KeepState.isOff = function(fieldID) {
  return 'off' === localStorage.getItem(fieldID);
};
KeepState.isEmpty = function(fieldID) {
  return null === localStorage.getItem(fieldID);
};
KeepState.isTaken = function(fieldID) {
  return !( KeepState.isEmpty(fieldID) || KeepState.isOff(fieldID) );
};
KeepState.mkFieldID = function(row, col) {
  return FiFi.fieldIdPrefix + ':' + String(row) + ':' + String(col);
};
// whether a figure (identified by `id`) is out of the garrison
KeepState.figureOut = function(id) {
  return id.match(/^[\w\-]+:\d+$/);
};
// if a field is taken, gets the figure that resides there
KeepState.figureOnField = function(field) {
  return field.getElementsByClassName(FiFi.figureClass)[0];
};
// helpers for event listeners to determine what they fired at
KeepState.isElemFigure = function(elem) {
  return FiFi.figureClass == elem.className;
};
KeepState.isElemField = function(elem) {
  return KeepState.isField(elem.id);
};
/* The id of a figure outside of the garrison contains a colon and a number
 * which makes the distinction between different figures of the same type on
 * the board.  HTML ids must be unique in the document, therefore no two
 * figures may have the same number.  Note that the full id is not saved to the
 * internal state (only the figure type, the part before the colon, is saved),
 * therefore reloading the page may shuffle id numbers.  Yet, if this function
 * is used for all figure placements and moves, it will ensure that all ids are
 * unique. */
KeepState.genID = function(figureID) {
  if (KeepState.figureOut(figureID))
    figureID = figureID.slice(0, figureID.indexOf(':'));
  var i = 0;
  do {
    var newID  = figureID + ':' + i;
    var exists = document.getElementById(newID);
    i++;
  } while (exists);
  return newID;
};
KeepState.reset = function() {
  localStorage.clear();
};
// clears the field from any previous state
KeepState.clearField = function(field) {
  localStorage.removeItem(field.id);
};
// wrapper, we might use some other kind of storage someday
KeepState.stringify = function() {
  return JSON.stringify(localStorage);
};
/* Flips the face of a field in the storage, does *not* check whether the field
 * is empty.  The face can be forced by passing the second parameter, this is
 * needed for the case when the board is drawn for the first time and the HTML
 * and the storage are in an inconsistent state to each other. */
KeepState.flipField = function(field, face) {
  if (!face) {
    if (KeepState.isOff(field.id)) face = 'on';
    else                           face = 'off';
  }
  if ('off' === face)
    localStorage.setItem(field.id, 'off');
  else
    localStorage.removeItem(field.id);
  KeepState.DEBUG(field.id, face, localStorage);
};
/* moves whatever storage content is on field `orig` to field `dest`,
 * does *not* check whether the `dest` field is taken or off */
KeepState.switch = function(orig, dest) {
  var content = localStorage.getItem(orig.id);
  if (content)  localStorage.setItem(dest.id, content);
  else          localStorage.removeItem(dest.id);
};
/* moves a figure from one field to another in the storage,
 * does *not* check whether the destination field is on */
KeepState.mvFig = function(figure, origField, destField) {
  var origID = '';
  if (origField) {
    origID = origField.id;
    localStorage.removeItem(origID);
  }
  var unitType = figure.id.split(':')[0];
  KeepState.DEBUG(unitType, origID, '->', destField.id);
  localStorage.setItem(destField.id, unitType);
  KeepState.DEBUG(localStorage);
};
// removes a figure on the board from the storage
KeepState.rmFig = function(figure, field) {
  var check = figure.parentNode;
  if (check.id != field.id)  // trivial assertion
    console.log('Removing the wrong figure:', field.id, check.id);
  localStorage.removeItem(field.id);
  KeepState.DEBUG('remove', field.id, localStorage);
};
/* Helper functions to add or remove several listeners in a clean way.
 * A `listeners` parameter shall be an object as follows:
 *  { 'event' : function() {...}
 *  , 'drag'  : function() {...}
 *  , 'click' : function() {...}
 *  } */
KeepState.addListeners = function(elem, listeners) {
  for (var evl in listeners) {
    KeepState.DEBUG(elem.id, 'add', evl);
    elem.addEventListener(evl, listeners[evl]);
  }
};
KeepState.delListeners = function(elem, listeners) {
  for (var evl in listeners) {
    KeepState.DEBUG(elem.id, 'del', evl);
    elem.removeEventListener(evl, listeners[evl]);
  }
};
/* `localStorage` is live, so we must convert it to
 * some kind of static array before we can delete things, boooo... */
KeepState.keys = function() {
  var keys = [];
  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    if (KeepState.isField(key))
      keys.push(key);
  }
  return keys;
};
// trivial check on the keys that we use
KeepState.isStateEmpty = function() {
  return 0 == KeepState.keys().length;
};
// load state from the URL query
KeepState.decodeURL = function(query) {
  query       = query.replace(/^\?/, '');
  var enc     = query.split('&');
  var storage = {};
  KeepState.DEBUG('decode', query);
  enc.forEach(function(comp) {
    var lr = comp.split('=');
    if (2 != lr.length) {
      KeepState.DEBUG('Bad query component:', comp);
      return;
    }
    var field   = decodeURIComponent(lr[0]);
    var content = decodeURIComponent(lr[1]);
    KeepState.DEBUG('add', field, '->', content);
    storage[field] = content;
  });
  return storage;
};
// convert the state into a URL
KeepState.encodeURL = function(loc) {
  var keys = KeepState.keys();
  var comp = [];
  keys.forEach(function(key) {
    var val   = localStorage.getItem(key);
    KeepState.DEBUG('add', key, '=', val);
    var enc = encodeURIComponent(key) + '=' + encodeURIComponent(val);
    KeepState.DEBUG('as', enc);
    comp.push(enc);
  });
  var query = '';
  if (0 < comp.length) query = '?' + comp.join('&');
  var url = loc.protocol + '//' + loc.host + loc.pathname + query + loc.hash;
  KeepState.DEBUG('final', url);
  return url;
};
/* Helper function that clears all storage of field:row:column IDs.
 * It uses `FiFi` to clear the display while clearing the storage.
 * Once finished `KeepState.clear` shall leave both the storage and
 * board clean of any Yarb state.  `localStorage` identifiers from other
 * sources are kept. */
KeepState.clear = function() {
  var keys = KeepState.keys();
  KeepState.DEBUG('All keys to clear:', keys);
  keys.forEach(function(fieldID) {
    var field  = document.getElementById(fieldID);
    if (KeepState.isOff(fieldID)) {
      KeepState.DEBUG('clear -> flip', fieldID);
      // flipField calls us back for storage cleaning
      FiFi.f.flipField(field, false);
    } else {
      var figure = KeepState.figureOnField(field);
      KeepState.DEBUG(localStorage.getItem(fieldID), 'killed at', fieldID);
      // rmFig calls us back for storage cleaning
      FiFi.r.rmFig(figure, field);
    }
  });
};
/* Helper function that draws the storage state on the board by means of
 * callbacks in the `FiFi` object.  It does not account for clearing the board
 * beforehand, therefore if a full redraw is needed `KeepState.clear` must be
 * called beforehand. */
KeepState.draw = function() {
  var keys = KeepState.keys();
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    KeepState.DEBUG('Process:', key);
    if (!KeepState.isField(key))
      continue;
    var field = document.getElementById(key);
    if (KeepState.isOff(key)) {
      KeepState.DEBUG('draw -> flip', field);
      // flipField calls us back for storage cleaning
      FiFi.f.flipField(field, true);
      continue;
    }
    var unitType = localStorage.getItem(key);
    var figure   = document.getElementById(unitType);
    if (!figure) {
       unitType = FiFi.figureUnknown;
       figure   = document.getElementById(unitType);
       localStorage.setItem(key, unitType);
    }
    KeepState.DEBUG(unitType, '->', key);
    // mvFig calls us back for storage cleaning
    FiFi.m.mvFig(figure, field);
  }
};
/* Filter out object keys that cannot be understood as fields.  This is a
 * security measure, if someone provides us with an object we do not know its
 * keys and therefore we definitely shall not update `localStorage` with a
 * bunch of random values.  It might appear to be a useless measure since
 * nothing in this file will use `localStorage` keys that are not proper
 * fields, yet this might not be the only file loaded on some page and other
 * files may use other `localStorage` identifiers.  We do not want to be
 * openers of a gate into someone else's piece of code, which may not consider
 * someone updating `localStorage` behind the scenes. */
KeepState.filterFields = function(storage) {
  var filtered = {};
  for (var key in storage) {
    KeepState.DEBUG('key:', key);
    if (KeepState.isField(key))
      filtered[key] = storage[key];
  }
  KeepState.DEBUG('Filtered:', filtered);
  return filtered;
};
/* the function that actually updates `localStorage` with new keys,
 * do not call it on objects that did not pass through the filter above */
KeepState.update = function(storage) {
  for (var key in storage)
    localStorage.setItem(key, storage[key]);
};
/* Helper function to update the board and the internal state from a new object
 * containing state.  The main use is to reload previously saved JSON data from
 * another interaction with the board. */
KeepState.reload = function(storage) {
  KeepState.clear();
  KeepState.update(storage);
  KeepState.draw();
};

/* `DragScroll` produces an element that can be scrolled by dragging, this
 * works best with elements with `overflow` set to `hidden`.  The micro-library
 * adds event listeners to the element `elem` and when activated the events
 * will scroll the element `toScroll`.  In most practical cases `elem` and
 * `toScroll` will be the same DOM element but it does not need to be.
 *
 * Most mobile browsers do not produce mouse events, instead we have the
 * following events:
 *   `mousedown` == `touchstart`
 *   `mousemove` == `touchmove`
 *   `mouseup`   == `touchend`
 * Unfortunately there is no equivalent for `mouseleave`, therefore on mobile
 * you can screw up things by not triggering `touchend`.  To work around this
 * `touchend` needs to be registered to `window` and stop all possible scrolls
 * when it happens.
 *
 * Even more unfortunately mobile browsers are not consistent in how they treat
 * the touch events.  The code commented out below works on `FirefoxOS` but
 * breaks `Chrome` on android.  And even more unfortunately i do not have the
 * resources to buy several devices to test a good solution for all of them.
 * Therefore, for the time being mobile browsers will need to use the arrows to
 * scroll the board and garrison.
 */
var DragScroll = {};
DragScroll.DEBUGON = false;
DragScroll.DEBUG   = function() {
  if (DragScroll.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['DragScroll:'].concat(args));
  }
};
// keep track of the mouse
DragScroll.mousedown = false;
DragScroll.mouseX    = 0;
DragScroll.mouseY    = 0;
/* One of the events registered in `DragScroll.register` is `mousedown`.  And
 * it has a `ev.preventDefault` call which disallows any events dependent on
 * `mousedown` (`click`, `dragstart`) from happening on on the children of a
 * registered element.  To allow children to receive these events `mousedown`
 * must never reach the registered element, for this purpose the following
 * callback needs to be added to `mousedown` event listeners in the children.
 * The same is valid for the `touchstart` event on mobile. */
DragScroll.mdownEv = function(ev) {
  // we need to stop propagation so that the drag scroll do not catch it
  FiFi.EVDEBUG('DragScroll: mousedown/touchstart at', ev.target);
  ev.stopPropagation();
};
DragScroll.mdownEvDflt = function(ev) {
  // same, but prevent default to prevent text selection in some cases
  FiFi.EVDEBUG('DragScroll: mousedown/touchstart at', ev.target);
  ev.stopPropagation();
  ev.preventDefault();
};
/* Unfortunately webkit is stupid and can scroll `window` only by
 * `scrollBy` whilst to scroll a `div` we must set `scrollTop` and
 * `scrollLeft`.  Gecko can do both ways on everything. */
DragScroll.scroll = function(elem, scrollX, scrollY) {
  DragScroll.DEBUG(scrollX, scrollY, elem);
  if (window === elem) {
    elem.scrollBy(scrollX, scrollY);
  } else {
    elem.scrollLeft = elem.scrollLeft + scrollX;
    elem.scrollTop  = elem.scrollTop  + scrollY;
  }
};
// register the scrolling element to the scrolled element
DragScroll.register = function(elem, toScroll) {
  if (!elem) {
    console.log(elem, ': no such element');
    return;
  }
  if (!toScroll) toScroll = elem;
  // We need to close over `elem` and `toScroll`
  var scrollElem = function(screenX, screenY) {
    scrollX = DragScroll.mouseX - screenX;  // reverse scrolling for good
    scrollY = DragScroll.mouseY - screenY;  // click and feel
    DragScroll.scroll(toScroll, scrollX, scrollY);
    DragScroll.mouseX = screenX;
    DragScroll.mouseY = screenY;
  };
  var scrollStart = function(ev) {
    FiFi.EVDEBUG('DragScroll: mousedown/touchstart at', elem.id);
    DragScroll.mousedown = true;
    DragScroll.mouseX    = ev.screenX;
    DragScroll.mouseY    = ev.screenY;
    /* Prevent default removes the possibility to drag images,
     * this is because 'mousedown' happens before 'drag'.  The workaround
     * is to register a 'mousedown' listener with `stopPropagation` on
     * all children that are meant to be draggable. */
    ev.preventDefault();
  };
  var scrollMove = function(ev) {
    FiFi.EVDEBUG( 'DragScroll: mousemove/touchmove at'
                , elem.id, DragScroll.mousedown );
    if (DragScroll.mousedown) scrollElem(ev.screenX, ev.screenY);
  };
  var scrollEnd = function(ev) {
    FiFi.EVDEBUG('DragScroll: mouseleave/up/touchend at', elem.id);
    if (DragScroll.mousedown) scrollElem(ev.screenX, ev.screenY);
    DragScroll.mousedown = false;
    DragScroll.mouseX    = 0;
    DragScroll.mouseY    = 0;
  };
  elem.addEventListener('mousedown',  scrollStart);
  elem.addEventListener('mousemove',  scrollMove);
  elem.addEventListener('mouseup',    scrollEnd);
  elem.addEventListener('mouseleave', scrollEnd);
  //elem.addEventListener('touchstart', scrollStart);
  //elem.addEventListener('touchmove',  scrollMove);
  // mobile hack (breaks mobile zoom, mobile browsers are shit)
  //window.addEventListener('touchend', scrollEnd);
};
/* register a simple scrolling arrow instead, this is a workaround needed
 * because of incompatibilities in mobile browsers. */
DragScroll.registerArrow = function(elem, toScroll, stepX, stepY) {
  elem.addEventListener('click', function(ev) {
    FiFi.EVDEBUG('DragScroll: click arrow');
    ev.preventDefault();
    DragScroll.scroll(toScroll, stepX, stepY);
  });
  // prevents the selection of the arrow as text
  elem.addEventListener('mousedown', function(ev) {
    ev.preventDefault();
  });
};

/* `EnumFields` is just used to enumerate all fields on the board.  It goes
 * through all fields keeping a counter and calls `KeepState.mkFieldID` to give
 * it a consistent id.
 */
var EnumFields = {};
EnumFields.DEBUGON = false;
EnumFields.DEBUG   = function() {
  if (EnumFields.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['EnumFields:'].concat(args));
  }
};
// note that `size` is the number of fields in one row
EnumFields.enumfld = function(base, size, fieldClass) {
  var allFields = base.getElementsByClassName(fieldClass);
  for (var i = 0; i < allFields.length; i++) {
    var col = i % size;
    var row = (i - col) / size;
    var fieldID = KeepState.mkFieldID(row, col);
    EnumFields.DEBUG('Elem', i, 'id', fieldID);
    allFields[i].id = fieldID;
  }
};

/* `YarbField` is a very simple micro-library that flips board fields by
 * changing their `class` attribute.  It allows for flipping by right clicking
 * the field through event listeners.  The flipped field state is maintained by
 * calling `KeepState`.
 *
 * Off fields shall not be interacted with but on fields may need interactions
 * through other event listeners, therefore the `listeners` parameter in the
 * constructor.  On fields will be updated to have the listeners whilst off
 * fields will have them removed.
 */
var YarbField = function(debug, listeners) {
  this.DEBUGON   = debug;
  this.listeners = listeners;
};
YarbField.prototype.DEBUG = function() {
  if (this.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['YarbField:'].concat(args));
  }
};
/* Flips the field on the board and calls `KeepState` to update the storage.
 * The `forceOff` parameter is needed for the special case of drawing the board
 * for the first time.  Since at the beginning all board HTML field are `on`,
 * these are inconsistent with the data in `localStorage`.  We need a way to
 * ignore this inconsistency and force turning the field face to the `off`
 * state, albeit the state tells us that it is already off. */
YarbField.prototype.flipField = function(field, forceOff) {
  this.DEBUG('flip', field);
  if (KeepState.isOff(field.id) && !forceOff) {
    KeepState.addListeners(field, this.listeners);
    field.className = FiFi.fieldOnClass;
    KeepState.flipField(field, 'on');
  } else {
    if (!KeepState.isTaken(field.id)) {
      KeepState.delListeners(field, this.listeners);
      field.className = FiFi.fieldOffClass;
      KeepState.flipField(field, 'off');
    } else {
      console.log(field.id, ': field not empty');
    }
  }
};
/* register a field to be flippable,
 * listeners are updated accordingly when flipped. */
YarbField.prototype.register = function(field) {
  if (!field) {
    console.log(field, ': no such element');
    return;
  }
  KeepState.addListeners(field, this.listeners);
  var self = this;
  /* There is a potential bug here in which the event triggers on one field and
   * is executed on another.  `ev.currentTarget` is used as defensive coding
   * against it. */
  field.addEventListener('contextmenu', function(ev) {
    FiFi.EVDEBUG('YarbField: right click at', ev.currentTarget);
    self.flipField(ev.currentTarget, false);
    ev.preventDefault();
  });
};

/* `YarbFigure` is a micro-library with the purpose of moving figures out of
 * the garrison to the board and around the board.  The original implementation
 * used purely drag and drop events, unfortunately several mobile browsers do
 * not support this and a selection method with pure `click` events was added
 * later.
 *
 * Just as the events in `YarbField` are used on fields, the events in
 * `YarbFigure` are be used mostly on figures.  For example, although right
 * clicking a figure and right clicking a field has a similar effect (both
 * disappear) the state changes are very different and need to be processed by
 * different handlers.
 *
 * On the other hand, `YarbFigure.dropFigure`, `YarbFigure.allowDrop`  and
 * `YarbFigure.clickField` are event listeners that must be set on the elements
 * where the figure can be placed, i.e. fields.  Therefore these listener
 * functions must be passed to `YarbField.register` in the `listeners`
 * argument.
 *
 * `YarbFigure` also depends on listeners from `DragScroll`, therefore it
 * stores these listeners as part of the object.  All these event handlers are
 * implemented by static methods.
 */
var YarbFigure = function(debug, listeners) {
  this.DEBUGON   = debug;
  this.listeners = listeners;
  this.selected  = null;  // keeps track of the selected figure
};
YarbFigure.prototype.DEBUG = function() {
  if (this.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['YarbFigure:'].concat(args));
  }
};
/* Whether there is a selected figure.  Or, if the `figure` parameter is given,
 * whether the given parameter is the currently selected figure. */
YarbFigure.prototype.isSelected = function(figure) {
  if (figure) return figure === this.selected;
  else        return !!this.selected;
};
// consider the given figure as selected
YarbFigure.prototype.select = function(figure) {
  figure.className = FiFi.figSelectedClass;
  this.selected    = figure;
  this.DEBUG('select', figure);
};
// deselect, the given figure or whatever figure is selected
YarbFigure.prototype.deselect = function(figure) {
  if (figure && figure != this.selected) {
    console.log('cannot deselect unselected figure');
    return false;
  }
  if (!this.selected) return true;
  this.selected.className = FiFi.figDeselectedClass;
  this.DEBUG('deselect', this.selected);
  this.selected = null;
  return true;
};
/* Actually moves the figure to the field, note that it does *not* check
 * whether the field is already taken.  Figures coming from the garrison must
 * be cloned otherwise they would disappear from the garrison.  Figures already
 * on the board are simply appended, since they shall disappear from their
 * original location. 
 *
 * If the first argument is `null` consider the call as moving a selected
 * figure.  We know where the selected is therefore it is not a problem to find
 * it.  If we call `mvFig` to move a selected figure without having a selected
 * figure, ignore the call; in theory such a situation shall never happen. */
YarbFigure.prototype.mvFig = function(figure, destField) {
  if (!figure) figure = this.selected;
  if (!figure) {
    console.log('no figure selected');
    return;
  }
  var origField = null;
  if (!KeepState.figureOut(figure.id)) {
    /* we are dropping a new figure on the field,
     * find a new id for it and clone */
    this.DEBUG('we need to clone id', figure.id);
    var newID = KeepState.genID(figure.id);
    var clone = figure.cloneNode(true);
    this.DEBUG('old id', clone.id);
    clone.id = newID;
    this.DEBUG('new id', clone.id);
    KeepState.addListeners(clone, this.listeners);
    clone.addEventListener('dragstart',   YarbFigure.dragStartFig);
    clone.addEventListener('click'  ,     YarbFigure.clickFigure);
    clone.addEventListener('contextmenu', YarbFigure.rClickFigure);
    clone.className = FiFi.figDeselectedClass;
    figure = clone;
  } else {
    origField = figure.parentNode;
  }
  KeepState.mvFig(figure, origField, destField);
  destField.appendChild(figure);
};
// removes the figure, cleans up selection if needed
YarbFigure.prototype.rmFig = function(figure, field) {
  KeepState.rmFig(figure, field);
  if (!figure) console.log('cannot find figure at', field.id);
  else         field.removeChild(figure);
  if (figure == this.selected)
    this.selected = null;
};
// register that needs to be run on all figures in garrison
YarbFigure.prototype.register = function(figure) {
  if (!figure) {
    console.log(figure, ': no such element');
    return;
  }
  KeepState.addListeners(figure, this.listeners);
  figure.addEventListener('dragstart', YarbFigure.dragStartFig);
  figure.addEventListener('click',     YarbFigure.clickFigure);
};
// helper function for event listeners, checks for occupied fields
YarbFigure.isTaken = function(figureOrField) {
  if (   KeepState.isElemFigure(figureOrField)
      || (   KeepState.isElemField(figureOrField)
          && null != KeepState.figureOnField(figureOrField)
         )
     ) {
    /* There is an element here already, ignore.
     * Note that the target may be the figure itself which is holding
     * the place, and the event simply propagated to this handler. */
    return true;
  }
  return false;
};
/* triggers figure removal by right click, thanks to the fact that the field
 * below the figure also responds to events we need to stop the propagation */
YarbFigure.rClickFigure = function(ev) {
  FiFi.EVDEBUG('YarbFigure: right click at', ev.target);
  ev.preventDefault();
  ev.stopPropagation();
  FiFi.r.rmFig(ev.target, ev.target.parentNode);
};
// selects and deselects figures
YarbFigure.clickFigure = function(ev) {
  ev.preventDefault();
  ev.stopPropagation();
  FiFi.EVDEBUG('YarbFigure: click at', ev.target);
  if (FiFi.i.isSelected(ev.target)){
    FiFi.d.deselect(ev.target);
  } else {
    FiFi.d.deselect();
    FiFi.s.select(ev.target);
  }
};
/* handler that shall be applied to fields,
 * it moves the selected figure (if any) to the clicked field */
YarbFigure.clickField = function(ev) {
  ev.preventDefault();
  ev.stopPropagation();
  FiFi.EVDEBUG('YarbFigure: click at', ev.target);
  if (!FiFi.i.isSelected()) return;
  if (YarbFigure.isTaken(ev.target)) {
    console.log('field occupied');
    return;
  }
  FiFi.m.mvFig(null, ev.target);
  FiFi.d.deselect();
};
/* the start of the drag and drop operation, we pass the id of the figure
 * being dragged to the handler that will deal with the drop of the figure */
YarbFigure.dragStartFig = function(ev) {
    FiFi.EVDEBUG('YarbFigure: dragstart at', ev.target);
    ev.dataTransfer.setData('text', ev.target.id);
};
/* listener to be applied to fields
 * which ensures that we only move figures onto empty fields */
YarbFigure.dropFigure = function(ev) {
  FiFi.EVDEBUG('YarbFigure: drop at', ev.target);
  ev.preventDefault();
  if (YarbFigure.isTaken(ev.target)) {
    console.log('field occupied');
    return;
  }
  var data = ev.dataTransfer.getData('text');
  FiFi.m.mvFig(document.getElementById(data), ev.target);
};
/* very simple listener function to be applied to fields, since the default of
 * all elements is to disallow drops we need to prevent this default */
YarbFigure.allowDrop  = function(ev) {
  FiFi.EVDEBUG('YarbFigure: dragover at', ev.target);
  ev.preventDefault();
};

/* `MoveBoard` is a micro-library responsible for moving the entire board in
 * some direction.  It performs this by moving the content and state of all
 * fields into the opposite direction by the same amount.
 *
 * It shall not be possible to move the board if that move would discard at
 * least one figure, since we have no way to track the fact that we lost that
 * figure.  If the board is moved into a direction which would discard figures
 * `MoveBoard` aborts and calls `FiFi` to display the information about the
 * issue to the user.
 */
var MoveBoard = {};
MoveBoard.DEBUGON = false;
MoveBoard.DEBUG   = function() {
  if (MoveBoard.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['MoveBoard:'].concat(args));
  }
};
/* walks through all combinations of rows and columns given
 * and returns the first taken field it finds, otherwise return `false` */
MoveBoard.checkFields = function(rows, cols) {
  for (var i = 0; i < rows.length; i++) {
    var r = rows[i];
    for (var j = 0; j < cols.length; j++) {
      var c       = cols[j];
      var fieldID = KeepState.mkFieldID(r, c);
      MoveBoard.DEBUG('Check', fieldID);
      if (KeepState.isTaken(fieldID)) return fieldID;
    }
  }
  return false;
};
/* moves whatever is on `orig` field into the `dest` field,
 * including field face */
MoveBoard.moveField = function(orig, dest) {
  // if there is nothing to do, just return
  if ( KeepState.isEmpty(orig.id) && KeepState.isEmpty(dest.id) ) return;
  if (KeepState.isOff(dest.id))
    FiFi.f.flipField(dest, false);
  if (KeepState.isTaken(dest.id)) {
    MoveBoard.DEBUG('Clear', dest.id);
    while (dest.firstChild) dest.removeChild(dest.fistChild);
  }
  KeepState.clearField(dest);
  if (KeepState.isOff(orig.id))
    FiFi.f.flipField(dest, false);
  if (KeepState.isTaken(orig.id)) {
    var figure = KeepState.figureOnField(orig);
    FiFi.m.mvFig(figure, dest);
  }
  /* We do not clear the `orig` field face because we want to keep it flipped
   * the same way as it was.  The new line of fields that appear has the fields
   * flipped the same way as the line that was moved.  On the other hand, all
   * figures are properly moved, including internal state cleaning. */
};
/* Moves all fields in a row (`vertical` == false) or column
 * (`vertical` == true) to the `next` row or column.  `next` is an offset of
 * how many fields are to be transversed (it will normally be +1 or -1). */
MoveBoard.moveRowCol = function(i, next, size, vertical) {
  for (var j = 0; j < size; j++) {
    var origID = null;
    var destID = null;
    if (vertical) {
      origID = KeepState.mkFieldID(j,      i);
      destID = KeepState.mkFieldID(j, i+next);
    } else {
      origID = KeepState.mkFieldID(i,      j);
      destID = KeepState.mkFieldID(i+next, j);
    }
    MoveBoard.DEBUG('MOVE', origID, '->', destID);
    MoveBoard.moveField( document.getElementById(origID)
                       , document.getElementById(destID) );
  }
};
/* Creates an function that can move the board in a direction.  It will move
 * each row (`vertical` == false) or column (`vertical` == true) starting from
 * `first` by the offset of `next`.  The row or column `last` will be checked
 * for figures since it will not be moved anywhere.  The `size` of the board is
 * needed to know the number of fields in a row or in a column.  The returned
 * function can be used repeatedly, therefore it can be bound to an event
 * handler */
MoveBoard.mkArrow = function(dir, vertical, first, next, last, size) {
  return function(ev) {
    // range(3) -> [0,1,2]
    var range = function(x) {
      return Array.apply(null, Array(x)).map(function(_,i) {return i;});
    };
    if (ev) ev.preventDefault();  // this might be an event handler
    MoveBoard.DEBUG('MOVE board', dir);
    var lastEmpty = true;
    if (vertical) lastEmpty = MoveBoard.checkFields(range(size), [last]);
    else          lastEmpty = MoveBoard.checkFields([last], range(size));
    if (lastEmpty) {
      FiFi.e.rndMsg('Cannot move ' + dir + ', there is a figure ' +
                    'at the other side of the board.  Remove the ' +
                    'figure at ' + lastEmpty + ' (field:row:column) ' +
                    'to more the board ' + dir + '.');
      return;
    }
    /* We need to go backwards to not overwrite existing figures.  We first
     * move the before last row or column onto the last one, then the before
     * before last onto the before last, and so on.  Once we reach the first
     * row or column we move it (which cleans figures) and leave the new row or
     * column as a copy of the first. */
    for (var i = last - next; next > 0 ? i >= first : i <= first; i -= next) {
      MoveBoard.moveRowCol(i, next, size, vertical);
    }
  };
};

/* `Dice` could be objects, but as they only need to keep a single value (the
 * number of sides) it is quicker to implement them as higher order functions.
 *
 * `Dice` is composed of several single dice commonly used in tabletop RPGs and
 * an expression evaluator to perform complex dice rolls.  For example, if the
 * result of rolling 7 dice of 6 sides minus 7 plus 3 dice of 8 sides plus 6
 * needs to be known it is easier to type 7d6-7+3d8+6 into the expression
 * evaluator than counting it by hand.  Of course if the players of the
 * tabletop game do have that many dice, it is often more fun to throw them and
 * count by hand.  The expression evaluator is there for the cases when the
 * number of dice on the table is not enough.
 *
 * The evaluator can also be used to perform as a dice not included in the
 * micro-library, e.g. 1d9, 1d18, or 3d16 are all valid expressions.
 */
var Dice = {};
Dice.DEBUGON = false;
Dice.DEBUG   = function() {
  if (Dice.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['Dice:'].concat(args));
  }
};
// Create different dice as closures
Dice.mkdice = function(sides) {
  return function() { return Math.ceil(Math.random() * sides) };
};
Dice.d4   = Dice.mkdice(4);
Dice.d6   = Dice.mkdice(6);
Dice.d8   = Dice.mkdice(8);
Dice.d10  = Dice.mkdice(10);
Dice.d12  = Dice.mkdice(12);
Dice.d20  = Dice.mkdice(20);
Dice.d100 = Dice.mkdice(100);
/* Parses and evaluates dice expressions,
 * 2d4 -> two times roll d4
 * 2d6+7+1d8-3+1d12 -> two times 6, once d8, once d12, plus 7 minus 3
 *
 * `upd` is a callback that will be called with the result of parsing
 * the expression given in `exp` */
Dice.dexp = function(exp, upd) {
  Dice.DEBUG('Exp to parse:', exp);
  exp = exp.toLowerCase();
  exp = exp.replace(/^\s+/, '');
  var negate = false;
  if (exp.match(/^-\d/))  // if there is a minus in front, keep it
    negate = true;
  exp = exp.replace(/^[\+\-\s]+/, '');
  exp = exp.replace(/[\+\-\s]+$/, '');
  if (negate) exp = '-' + exp;
  else        exp = '+' + exp;
  Dice.DEBUG('Cleaned:', exp);
  var plus  = [];
  var minus = [];
  var regex = /([\-\+])([0-9d]+)/g;
  var match;
  while (match = regex.exec(exp)) {
    if ('-' == match[1]) {
      minus.push(match[2]);
    } else if ('+' == match[1]) {
      plus.push(match[2]);
    } else {
      Dice.DEBUG('Bad match:', match);
    }
  }
  var parsed_plus = '';
  var parsed_minus = '';
  if (0 < plus.length)
    var parsed_plus  =  plus.reduce(function(p, elem) { return p += '+'+elem});
  if (0 < minus.length)
    var parsed_minus = minus.reduce(function(p, elem) { return p += '-'+elem});
  var parsed = parsed_plus + (0 < minus.length ? '-' : '') + parsed_minus;
  Dice.DEBUG('Parsed as:', parsed);
  if (upd)
    upd(parsed);
  var eval_dice = function(elem) {
    if (-1 == elem.indexOf('d'))
      return Number(elem);  // not a dice
    var m = elem.match(/([0-9]+)d([0-9]+)/);
    var total = 0;
    var times = Number(m[1]);
    var sides = Number(m[2]);
    for (var i = 0; i < times; i++)
      total += Math.ceil(Math.random() * sides);
    Dice.DEBUG(elem, '->', total);
    return total;
  };
  Dice.DEBUG('Positives:', plus);
  Dice.DEBUG('Negatives:', minus);
  plus  = plus.map(eval_dice);
  minus = minus.map(eval_dice);
  Dice.DEBUG('Positives:', plus);
  Dice.DEBUG('Negatives:', minus);
  var sum = function(s, t) { return t + s };
  var totp = plus.reduce(sum, 0);
  var totm = minus.reduce(sum, 0);
  return totp - totm;
};
/* Builds a handler to roll a clicked dice.  It works for both , the single
 * dice and the dice expression since the single dice will ignore `null`
 * arguments.  It also adds extra listeners to the dice and expression inputs.
 * Since an expression must come from some kind of text field it is safe to
 * assume it is a DOM element. */
Dice.rolld = function( link, dice, result, max
                     , diceListeners, exp, upd, expListeners ) {
  link.addEventListener('click', function(ev) {
    ev.preventDefault();
    var arg = null;
    if (exp)
      arg = exp.value;
    var ret = dice(arg, upd);
    result.textContent = '0';  // ensures that the user see the dice rolling
    setTimeout(function() { result.textContent = String(ret % max) }, 300);
  });
  if (diceListeners)       KeepState.addListeners(link, diceListeners);
  if (exp && expListeners) KeepState.addListeners(exp, expListeners);
};

/* `PageLinks` deals with the dialogs on the page.  This is the least portable
 * of all micro-libraries because it depends on IDs inside the HTML.  Since
 * different pages will have completely different dialogs, a generic dialog
 * library would be a bigger project than YARB itself.  On the other hand some
 * trivial higher order functions are generic enough to be reusable in some
 * circumstances.
 *
 * Several dialogs deal with specific features: resetting the board, saving
 * state to a file and state upload.  The code for these features is
 * independent from the board fields or figures, therefore it is included as
 * part of `PageLinks`.
 */
var PageLinks = {};
PageLinks.DEBUGON = false;
PageLinks.DEBUG   = function() {
  if (PageLinks.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['PageLinks:'].concat(args));
  }
};
// helper to create hide and show of elements as no argument functions
PageLinks.showHide = function(id, display) {
  return function() {
    PageLinks.DEBUG(id, '->', display);
    document.getElementById(id).style.display = display;
  };
};
PageLinks.sBlur   = PageLinks.showHide('message-blur', 'block');
PageLinks.hBlur   = PageLinks.showHide('message-blur', 'none' );
PageLinks.sMsg    = PageLinks.showHide('msg-msg',      'block');
PageLinks.hMsg    = PageLinks.showHide('msg-msg',      'none' );
PageLinks.sQuery  = PageLinks.showHide('query-msg',    'block');
PageLinks.hQuery  = PageLinks.showHide('query-msg',    'none' );
PageLinks.sReset  = PageLinks.showHide('reset-msg',    'block');
PageLinks.hReset  = PageLinks.showHide('reset-msg',    'none' );
PageLinks.sUpload = PageLinks.showHide('upload-msg',   'block');
PageLinks.hUpload = PageLinks.showHide('upload-msg',   'none' );
PageLinks.sSave   = PageLinks.showHide('save-msg',     'block');
PageLinks.hSave   = PageLinks.showHide('save-msg',     'none' );
PageLinks.sPause  = PageLinks.showHide('pause-msg',    'block');
PageLinks.hPause  = PageLinks.showHide('pause-msg',    'none' );
PageLinks.sHelp   = PageLinks.showHide('help-msg',     'block');
PageLinks.hHelp   = PageLinks.showHide('help-msg',     'none' );
PageLinks.sAbout  = PageLinks.showHide('about-msg',    'block');
PageLinks.hAbout  = PageLinks.showHide('about-msg',    'none' );
/* A dialog is constructed by adding several functions to certain clickable
 * elements (buttons, links).  It shall be passed an object with a set of
 * element IDs each of which pointing to an array of callbacks.  For example:
 *  { 'id'           : [ fun , fun , fun ]
 *  , 'button-open'  : [ showBox , getFile ]
 *  , 'button-close' : [ hideBox ]
 *  }
 * The callbacks will be called in the same order in which they are defined,
 * and they will also be given the click event object as their first argument.
 * The limitation of this function is that we can only show and hide existing
 * dialogs, since there is no way to directly pass content to a dialog. */
PageLinks.mkDialog = function(clicks) {
  Object.keys(clicks).forEach(function(elemID) {
    var callbacks = clicks[elemID];
    document.getElementById(elemID).addEventListener('click', function(ev) {
      callbacks.forEach(function(c) { c(ev) });
    });
  });
};
// all dialogs that need to prevent default can use this
PageLinks.pdef = function(ev) {
  ev.preventDefault();
};
// pause dialog, just a close button
PageLinks.pause = {
  'pause-link'      : [ PageLinks.sBlur , PageLinks.sPause , PageLinks.pdef ]
, 'pause-msg-close' : [ PageLinks.hBlur , PageLinks.hPause , PageLinks.pdef ]
};
// help dialog, again just a close button
PageLinks.help = {
  'help-link'      : [ PageLinks.sBlur , PageLinks.sHelp , PageLinks.pdef ]
, 'help-msg-close' : [ PageLinks.hBlur , PageLinks.hHelp , PageLinks.pdef ]
};
// about dialog, close button
PageLinks.about = {
  'about-link'      : [ PageLinks.sBlur , PageLinks.sAbout , PageLinks.pdef ]
, 'about-msg-close' : [ PageLinks.hBlur , PageLinks.hAbout , PageLinks.pdef ]
};
// the reset dialog clears the board
PageLinks.resetOp = function() {
  PageLinks.DEBUG('RESET taking place');
  KeepState.clear();
  FiFi.d.deselect();
};
PageLinks.reset = {
  'reset-link'       : [ PageLinks.sBlur , PageLinks.sReset , PageLinks.pdef ]
, 'reset-msg-cancel' : [ PageLinks.hBlur , PageLinks.hReset , PageLinks.pdef ]
, 'reset-msg-reset'  : [ PageLinks.hBlur
                       , PageLinks.hReset
                       , PageLinks.pdef
                       , PageLinks.resetOp
                       ]
};
/* The upload dialog is more complex because the upload button needs to be
 * disabled while the file is being loaded.  That includes removing all event
 * listeners.  Since event listeners created in `PageLinks.mkDialog` are
 * anonymous functions they cannot be removed and we need to resort to a dirty
 * trick.  Instead of removing the listeners we clone the upload button (which
 * does not clone listeners) and substitute the real button for the clone.
 *
 * When substituted the clone of the upload button is greyed out.  During this
 * time we have the opportunity to add some animation to keep the waiting user
 * entertained.  After the file is fully uploaded and processed, the animation
 * shall end and we need to register the listeners back into the button, and
 * enable it.  Since this all can take some time and can fail, we need to be
 * careful about properly finishing things. */
PageLinks.uploading = false;  // track animation, just in case
// the CSS3 animation
PageLinks.uploadAnim = function() {
  PageLinks.DEBUG('Animation starts');
  var fileID   = 'upload-file';
  var uploadID = 'upload-msg-upload';
  var cancelID = 'upload-msg-cancel';
  [ uploadID , cancelID ].forEach(function(id) {
    var a     = document.getElementById(id);
    var clone = a.cloneNode(true);
    a.parentNode.replaceChild(clone, a);
    clone.addEventListener('click', PageLinks.pdef);
    clone.className = FiFi.buttonOff;
    clone.id        = id;  // just in case
  });
  document.getElementById(uploadID).textContent = 'Uploading...';
  document.getElementById(fileID).disabled      = true;
  PageLinks.uploading = true;
};
PageLinks.uploadStop = function() {
  PageLinks.DEBUG('Animation ends');
  var fileID   = 'upload-file';
  var uploadID = 'upload-msg-upload';
  var cancelID = 'upload-msg-cancel';
  var dialog   = {};
  dialog[cancelID] = [ PageLinks.hBlur , PageLinks.hUpload , PageLinks.pdef ]
  dialog[uploadID] = [ PageLinks.uploadOp , PageLinks.hBlur
                     , PageLinks.hUpload  , PageLinks.pdef  ];
  [ uploadID , cancelID ].forEach(function(id) {
    var a       = document.getElementById(id);
    a.className = FiFi.buttonOn;
    a.removeEventListener('click', PageLinks.pdef);
  });
  document.getElementById(uploadID).textContent = 'Upload';
  document.getElementById(fileID).disabled      = false;
  PageLinks.mkDialog(dialog);
  PageLinks.uploading = false;
};
// the file operations happen in here
PageLinks.uploadOp = function() {
  PageLinks.DEBUG('UPLOAD taking place');
  if (PageLinks.uploading) return;
  var input = document.getElementById('upload-file');
  var file  = null;
  if (input.files.length > 0) {
    file = input.files[0];
    PageLinks.DEBUG('Will read', file.name);
    PageLinks.DEBUG('MIME', file.type);
  } else {
    PageLinks.rndMsg('You must select a file to upload.');
    return;
  }
  PageLinks.uploadAnim();
  var reader = new FileReader();
  reader.addEventListener('error', function(ev) {
    PageLinks.uploadStop();
    /* this probably is not a good error description but the standard does not
     * have a consistent error description for file events */
    PageLinks.rndMsg('Could not read file', String(ev.type));
  });
  // just out of curiosity, add a progress report
  reader.addEventListener('progress', function(data) {
    if (data.lengthComputable) {
      // `parseInt` is safer here than `Number`, browsers may be lying to us
      var progress = parseInt(((data.loaded/data.total) * 100), 10);
      console.log('Upload progress (%):', progress);
    }
  });
  reader.addEventListener('load', function(ev) {
    PageLinks.uploadStop();
    PageLinks.DEBUG('Parsing file');
    try {
      var storage = JSON.parse(reader.result);
    } catch (e) {
      PageLinks.rndMsg('Could not parse file:', e.toString());
      return;
    }
    var filtered = KeepState.filterFields(storage);
    if (0 >= Object.keys(filtered).length) {
      PageLinks.rndMsg('File does not contain meaningful data or is empty, ' +
                       'i will not update the board');
      return;
    }
    PageLinks.DEBUG('Clearing and redrawing');
    KeepState.reload(filtered);
  });
  reader.readAsText(file);
};
PageLinks.upload = {
  'upload-link'       : [ PageLinks.sBlur,PageLinks.sUpload,PageLinks.pdef ]
, 'upload-msg-cancel' : [ PageLinks.hBlur,PageLinks.hUpload,PageLinks.pdef ]
, 'upload-msg-upload' : [ PageLinks.pdef
                        , PageLinks.hBlur
                        , PageLinks.hUpload
                        , PageLinks.uploadOp
                        ]
};
/* We need the file and the url on the save links at the moment the user opens
 * the dialog, therefore we need to do extra work before it shows up.  The
 * creation of the link is not a very expensive procedure for the size of the
 * YARB board and no network is involved, therefore there is no need to disable
 * the save link while the link is being created. */
PageLinks.saveOp = function() {
  PageLinks.DEBUG('Building URL to save');
  var data    = KeepState.stringify();
  var jsonURL = 'data:text/json;charset=utf8,';
  /* base64 cannot be used because most browsers still use UTF-16 for DOM
   * encodings (including `localStorage`) whilst URLs can only be in UTF-8.
   * A UTF-8 URL may kick in browser security measures against domain
   * obfuscation (yes, that's stupid on a data URL but happens), therefore
   * there is no good solution.  We use `encodeURIComponent` as it is less
   * common for browsers to have security issues with data URLs than random
   * users adding UTF-8 characters to `localStorage` for unknown reasons. */
  jsonURL       += encodeURIComponent(data);
  var jsonA      = document.getElementById('save-msg-save');
  jsonA.download = 'yarb-' + Date.now().toString() + '.json';
  jsonA.href     = jsonURL;
  var queryURL   = KeepState.encodeURL(location);
  var queryA     = document.getElementById('save-msg-url');
  queryA.href    = queryURL;
};
PageLinks.save = {
  'save-link'       : [ PageLinks.sBlur , PageLinks.sSave
                      , PageLinks.pdef  , PageLinks.saveOp ]
, 'save-msg-cancel' : [ PageLinks.hBlur , PageLinks.hSave , PageLinks.pdef ]
// we cannot prevent default on the save link
, 'save-msg-save'   : [ PageLinks.hBlur , PageLinks.hSave ]
, 'save-msg-url'    : [ PageLinks.hBlur , PageLinks.hSave ]
};
// store the URL query, a wrapper for non-browser environments
PageLinks.urlQuery = '';
// updates the state from the URL query, does *not* perform a redraw
PageLinks.fromQuery = function() {
  PageLinks.DEBUG('Trying to load scene from URL query', PageLinks.urlQuery);
  if ('' == PageLinks.urlQuery) return;  // nothing to do
  var storage = KeepState.decodeURL(PageLinks.urlQuery);
  if (!storage) {
    PageLinks.rndMsg('Bad URL query, loading scene from cache');
    return;
  }
  var filtered = KeepState.filterFields(storage);
  if (0 >= Object.keys(filtered).length) {
    PageLinks.rndMsg('The URL query does not contain meaningful data, ' +
                     'loading scene from cache');
    return;
  }
  // we need the full reload since this executes after the first draw
  KeepState.reload(storage);
};
// the dialog that takes the user's decision about state load
PageLinks.showQueryDialog = function() {
  PageLinks.DEBUG('Show query dialog');
  PageLinks.sBlur();
  PageLinks.sQuery();
};
PageLinks.query = {
  'query-msg-cache' : [ PageLinks.hBlur , PageLinks.hQuery , PageLinks.pdef ]
, 'query-msg-query' : [ PageLinks.hBlur , PageLinks.hQuery
                      , PageLinks.pdef  , PageLinks.fromQuery ]
};
/* Since we might need to present different messages in a dialog depending on
 * the internal state we also need a generic dialog.  This generic dialog takes
 * all its arguments and present each as a paragraph inside a generic dialog,
 * identified by `fail-msg-text`. */
PageLinks.rndMsg = function() {
  var div = document.getElementById('fail-msg-text');
  while (div.firstChild) div.removeChild(div.firstChild);
  var args = Array.prototype.slice.call(arguments);
  args.forEach(function(arg) {
    var p = document.createElement('p');
    var t = document.createTextNode(arg);
    p.appendChild(t);
    div.appendChild(p);
  });
  PageLinks.sBlur();
  PageLinks.sMsg();
};
/* when we hide the generic dialog update it with a message
 * that shall never appear to the user */
PageLinks.hideMsg = function() {
  PageLinks.hMsg();
  // use the invisible clone in the HTML
  var textID  = 'fail-msg-text';
  var cloneID = 'clone-msg-text';
  var div     = document.getElementById(textID);
  var clone   = document.getElementById(cloneID).cloneNode(true);
  div.parentNode.replaceChild(clone, div);
  clone.id = textID;
};
// we can still bind the close button on the generic dialog
PageLinks.rndDialog = {
  'msg-msg-close' : [ PageLinks.hBlur , PageLinks.hideMsg , PageLinks.pdef ]
};

/* The `Test` object contains several unit tests for the micro-libraries above.
 * The object is never called during the normal execution of YARB, therefore
 * you can remove it altogether in production code.
 *
 * To run a test you need to call the text function explicitly from the
 * console.  Note that tests expect a clean board, moreover, several tests do
 * clean the board and will destroy any scene currently taking place on it.
 */
var Test = {};
Test.DEBUGON = true;
Test.DEBUG   = function() {
  if (Test.DEBUGON) {
    var args = Array.prototype.slice.call(arguments);
    console.log.apply(console, ['Test:'].concat(args));
  }
};
// keep test results to output a summary at the end of a set of tests
Test.allOK = true;
Test.good  = [];
Test.fail  = [];
Test.begin = function() {
  Test.allOK = true;
  Test.good  = [];
  Test.fail  = [];
};
Test.update = function(name, result) {
  if (result) Test.good.push(name);
  else        Test.fail.push(name);
};
Test.end = function() {
  Test.DEBUG(Test.good);
  Test.DEBUG(Test.fail);
  var good = Test.good.length;
  var fail = Test.fail.length;
  console.log(good, 'out of', good+fail, 'tests passed');
  if (0 < fail) console.log('FAILED tests:', Test.fail.join(', '));
};
/* Simple assert that compares return values, note that the function runs
 * before the assert therefore exceptions will explode the test.  `equal`
 * (if true) means that `ret` and `fun` must be the same for the test to pass,
 * otherwise (`equal` == false) `ret and `fun` must be different. */
Test.assert = function(name, equal, ret, fun) {
  var through = false;
  if (ret === fun) through = false;
  else             through = true;
  if (equal) through = !through;
  if (through) Test.DEBUG('Test', name, 'SUCCESS');
  else        console.log('Test', name, 'FAIL');
  Test.update(name, through);
  return through;
};
/* Assert for exceptions, any exceptions will be caught without checking for
 * the correct instances.  `pass` (if true) expects the function to not throw
 * an exception, otherwise (`pass` == false) the function shall throw an
 * exception for the test to pass. */
Test.assertEx = function(name, pass, callback) {
  var through = false;
  try {
    callback();
    through = true;
  } catch(e) {
    through = false;
  }
  if (pass) through = !through;
  if (through) Test.DEBUG('Test', name, 'SUCCESS');
  else        console.log('Test', name, 'FAIL');
  Test.update(name, through);
  return through;
};
// tests for the KeepState micro-library
// TODO modify the libraries to allow for easier testing.
Test.keepState = function(debug) {
  if (debug) KeepState.DEBUGON = true;
  Test.DEBUG('Tests.testKeepState');
  Test.begin();

  Test.assert('isField:1', true, false, KeepState.isField('bad-id'));
  Test.assert('isField:2', true, false, KeepState.isField('field:1'));
  Test.assert('isField:3', true, false, KeepState.isField('field:1:1:1'));
  Test.assert('isField:4', true, false, KeepState.isField('field:-1:1'));
  Test.assert('isField:5', true, false, KeepState.isField('field:c:d'));
  Test.assert('isField:6', true, true,  KeepState.isField('field:1:1'));

  localStorage.clear();
  localStorage.setItem('field:1:1', 'off');
  localStorage.setItem('field:2:2', FiFi.figureUnknown);
  Test.assert('isOff:1', true, true,  KeepState.isOff('field:1:1'));
  Test.assert('isOff:2', true, false, KeepState.isOff('field:2:2'));
  Test.assert('isOff:3', true, false, KeepState.isOff('field:3:3'));

  localStorage.clear();
  localStorage.setItem('field:1:1', 'off');
  localStorage.setItem('field:2:2', FiFi.figureUnknown);
  localStorage.setItem('field:2:2', 'bad-figure');
  Test.assert('isEmpty:1', true, false, KeepState.isEmpty('field:1:1'));

  localStorage.clear();
  Test.assertEx('exception', true, function() { throw Error('opsie')});

  Test.end();
};

/* We use `module.exports` to allow import into other Javascript environments.
 * Although most of the code will not work outside of the browser, some may be
 * reused if we add some `module.exports` tricks.  Without this any other
 * Javascript environment would simply complain that `window` and `document`
 * variables cannot be found.
 */
if ('undefined' === typeof module) var module = {};
module.exports = function(win) {
  win.addEventListener('load', function(ev) {
    // prepare an object for registering fields
    var yarbField = new YarbField(false, { 'drop'     : YarbFigure.dropFigure
                                         , 'dragover' : YarbFigure.allowDrop
                                         , 'click'    : YarbFigure.clickField
                                         });
    // prepare an object for registering figures
    var yarbFigure = new YarbFigure( false
                                   , { 'mousedown' : DragScroll.mdownEv } );
    // update `FiFi` with all needed objects, now we can rock
    FiFi.f = yarbField;
    FiFi.m = yarbFigure;
    FiFi.r = yarbFigure;
    FiFi.i = yarbFigure;
    FiFi.s = yarbFigure;
    FiFi.d = yarbFigure;
    FiFi.e = PageLinks;

    // scroll by dragging for desktop browsers
    DragScroll.register(document.getElementById('left-pane'));
    DragScroll.register(document.getElementById('footer'));
    DragScroll.register(document.getElementById('main-page'), window);
    // scroll arrows for mobile (the step was determined by trial and error)
    var scrollStep = 180;
    DragScroll.registerArrow( document.getElementById('scroll-board-a-up')
                            , window,           0, -scrollStep );
    DragScroll.registerArrow( document.getElementById('scroll-board-a-left')
                            , window, -scrollStep,           0 );
    DragScroll.registerArrow( document.getElementById('scroll-board-a-right')
                            , window,  scrollStep,           0 );
    DragScroll.registerArrow( document.getElementById('scroll-board-a-down')
                            , window,           0,  scrollStep);
    DragScroll.registerArrow( document.getElementById('scroll-garrison-a-up')
                            , document.getElementById('left-pane')
                            , 0, -scrollStep);
    DragScroll.registerArrow( document.getElementById('scroll-garrison-a-down')
                            , document.getElementById('left-pane')
                            , 0,  scrollStep);

    // add IDs to all fields so we can reference them
    EnumFields.enumfld( document.getElementById('board')
                      , 16, FiFi.fieldOnClass );

    // all fields start as `on`, use that to register handlers
    var colorFields = document.getElementsByClassName(FiFi.fieldOnClass);
    var colLen      = colorFields.length;
    for (var i = 0; i < colLen; i++) {
      yarbField.register(colorFields[i]);
    }

    // all needed listeners on the figures
    var figImages = document.getElementsByClassName(FiFi.figureClass);
    var figLen    = figImages.length;
    for (var i = 0; i < figLen; i++) {
      yarbFigure.register(figImages[i]);
    }

    // bind the arrows that move the board
    var clArr = function(id, fun) {
      document.getElementById(id).addEventListener('click', fun);
    };
    // mkArrow(dir, vertical, first, next, last, size)
    clArr('go-left-link',  MoveBoard.mkArrow('left',  true,   0,  1, 15, 16));
    clArr('go-right-link', MoveBoard.mkArrow('right', true,  15, -1,  0, 16));
    clArr('go-up-link',    MoveBoard.mkArrow('up',    false,  0,  1, 15, 16));
    clArr('go-down-link',  MoveBoard.mkArrow('down',  false, 15, -1,  0, 16));

    // single dice
    var diceListeners = { 'mousedown' : DragScroll.mdownEvDflt };
    Dice.rolld( document.getElementById('dice-d4'),   Dice.d4
              , document.getElementById('result-d4'),      10, diceListeners );
    Dice.rolld( document.getElementById('dice-d6'),   Dice.d6
              , document.getElementById('result-d6'),      10, diceListeners );
    Dice.rolld( document.getElementById('dice-d8'),   Dice.d8
              , document.getElementById('result-d8'),      10, diceListeners );
    Dice.rolld( document.getElementById('dice-d10'),  Dice.d10
              , document.getElementById('result-d10'),    100, diceListeners );
    Dice.rolld( document.getElementById('dice-d12'),  Dice.d12
              , document.getElementById('result-d12'),    100, diceListeners );
    Dice.rolld( document.getElementById('dice-d20'),  Dice.d20
              , document.getElementById('result-d20'),    100, diceListeners );
    Dice.rolld( document.getElementById('dice-d100'), Dice.d100
              , document.getElementById('result-d100'),  1000, diceListeners );
    // dice expression
    Dice.rolld( document.getElementById('dice-dexp'), Dice.dexp
              , document.getElementById('result-dexp'),  1000, diceListeners
              , document.getElementById('dice-input'), function(upd) {
                document.getElementById('dexp-parsed').textContent = upd; }
              , { 'mousedown' : DragScroll.mdownEv } );

    // simple dialogs
    PageLinks.mkDialog(PageLinks.pause);
    PageLinks.mkDialog(PageLinks.help);
    PageLinks.mkDialog(PageLinks.about);
    // complex dialogs
    PageLinks.mkDialog(PageLinks.reset);
    PageLinks.mkDialog(PageLinks.upload);
    PageLinks.mkDialog(PageLinks.save);
    // the random (error) message close buttons
    PageLinks.mkDialog(PageLinks.rndDialog);
    // the user dialog about URL queries (see below)
    PageLinks.mkDialog(PageLinks.query);

    // store the URL query
    PageLinks.urlQuery = win.location.search;
    // no previous state, try to load from URL query
    if (KeepState.isStateEmpty())      PageLinks.fromQuery();
    // we have a URL query and some previous state, ask the user what to do
    else if ('' != PageLinks.urlQuery) PageLinks.showQueryDialog();

    /* This keeps the state in localStorage,
     * the 'beforeunload' event is not needed anmore. */
    KeepState.draw();
    /*addEventListener('beforeunload', function(ev) {
      // this is not displayed anymore in most browsers
      var confirmation = 'wow!';
      ev.returnValue = confirmation;  // Gecko, Trident, Chrome 34+
      return confirmation;            // Gecko, WebKit, Chrome <34
    });*/
  });
};
module.exports.FiFi       = FiFi;
module.exports.KeepState  = KeepState;
module.exports.DragScroll = DragScroll;
module.exports.EnumFields = EnumFields;
module.exports.YarbField  = YarbField;
module.exports.YarbFigure = YarbFigure;
module.exports.MoveBoard  = MoveBoard;
module.exports.Dice       = Dice;
module.exports.PageLinks  = PageLinks;
module.exports.Test       = Test;
// if we are in the browser run everything, otherwise stay put
if ('undefined' !== typeof window) module.exports(window);

