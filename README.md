README for YARB
===============

yarb - Yet another RPG board

Copying
-------

Copyright (C) 2016 Michal Grochmal

This file is part of yarb.

yarb is free software; you can redistribute and/or modify YARB under the terms
of the GNU General Public License as published by the Free Software Foundation;
either version 3 of the License, or (at your option) any later version.

yarb is distributed in the hope that they will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

The COPYING file in the root directory of the project contains a copy of the
GNU General Public License.  If you cannot find this file, see
<http://www.gnu.org/licenses/>.

Acknowledgements
----------------

First of all thanks for the people at the Birkbeck College London to make me
play RPG once again.  The fact that i needed an electronic board is the main
and only reason i wrote it.

Many thanks for the community of the [wesnoth][] game.  Given the fact that i
draw (very) badly their art was indispensable.  Thanks especially to the fact
that they decided to license all the art under GPL, allowing spinouts (like
this one) to reuse the sprites.

[wesnoth]: https://www.wesnoth.org/

TODOs
-----

* Build the `Test` system properly, add at least some unit tests for each micro
  library.
* Modify several of the micro-libraries to allow for easier testing (e.g. add
  meaningful return values even for functions that do not need to return
  anything).
* Try a `div` with hidden overflow instead of building the board directly in
  the `body` of the document.  This might look better on mobile.

